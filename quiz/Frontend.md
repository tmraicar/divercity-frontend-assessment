## **Question 1:**

How would you speed up a single page application?
some ways to speed up single page applications are as follows:
SPA monitoring
Lazy rendering
Lazy data fetching
Static content caching
WebSockets
Bypassing the same-origin policy
Content Delivery Networks (CDN)

## **Question 2:**

Explain the `this` keyword and it's quirks in JavaScript
this keyword refers to the object
when used within a class, you can acceess all the object's properties and methods
this can be used with call() and apply() functions.The call method takes the parameters and applies them to an array
this can also be called within a function

## **Question 3:**

What is the difference between border-box and content-box?
content-box gives you the default CSS box-sizing. 
border-box tells the browser to account for any specific border and padding values that you give for an element's width and height.

## **Question 4:**

What's the difference between == and === operators?
== is type coercion and converts the variable values to the same type before performing comparison. 
=== is strict equality and returns true only if both values and types are identical for the two variables being compared.

## **Question 5:**

What is accessibility? How do you achieve it?
Accesibility is making your website accessible and usable for people of all abilities. 
some ways to achieve accesibility are:
Choose a content management system that supports accessibility
Use headings correctly to organize the structure of your content
Include proper alt text for images
descriptive names for links
Careful choice of color
Design your forms for accessibility
Ensure that all content can be accessed with the keyboard alone in a logical way
Make dynamic content accessible
Delivering content in more than one way, such as by text-to-speech or by video.
Website authentication as easy as possible without compromising security.

## **Question 6:**

What is the difference between session storage, local storage and cookies?
localStorage is a way to store data on the client’s computer. 
session storage stores data only for a session, until the browser (or tab) is closed.
cookies store data that has to be sent back to the server with subsequent XHR requests and expire based on time set either on client or server side.

## **Question 7:**

How does hoisting work in JavaScript, and what is the order of hoisting?
JavaScript Hoisting is a process whereby the interpreter appears to move the declaration of functions, variables or classes to the top of their scope, 
prior to execution of the code. Hoisting allows functions to be safely used in code before they are declared.

## **Question 8:**

What is the difference in usage of callback, promise, and async/await?
Promise is an object representing intermediate state of operation which is guaranteed to complete its execution at some point in future. 
Async/Await is a syntactic sugar for promises, a wrapper making the code execute more synchronously.
Callback is a function that is passed to another function. When the first function is done, it will run the second function.