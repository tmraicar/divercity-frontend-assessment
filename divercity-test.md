# **Divercity Full-Stack Engineer Challenge**

Thanks for taking the time to complete this exercise. We are excited that you are considering joining Divercity.  

In this test, you are expected to write a small web application to manage a list of departments in a company. Each department has a name, set of teams, and the person in-charge. Each team has a team number, a set of people, and a team lead. 
The test consists of two parts, a RESTful or GraphQL API as the **backend** and the JavaScript based **frontend** application.  

There is no need to create a PR back to this repository once completed, please provide a link to your project repository for review.  

## Backend implementation

Use the following structure to model the data  

```
class Department(Model):
    department_id: integer 
    name: string
    teams: list<team_id> (nullable)
    inCharge: <user_id>
```  

```
class Team(Model):
    team_id: integer
    people: list<user_id> (nullable)
    teamLead: user_id 
```  

```
class User(Model):
    user_id: integer
    name: string 
```  

Implement the following API endpoints:  

* **GET /departments/** - Returns a list of departments in the database in JSON format
* **GET /departments/{{id}}/** - Returns a detail view of the specified department. Including department, and team IDs in JSON format
* **GET /teams/** - Returns a list of teams in the database in JSON format
* **GET /teams/{{id}}/** - Returns a detail view of the specified team id
* **GET /users/** - Returns a list of users in the database in JSON format
* **GET /users/{{id}}/** - Returns a detail view of the specified user id  
*
* **POST /departments/** - Creates a new department with the specified details - Expects a JSON body
* **POST /teams/** - Creates a new team with the specified details - Expects a JSON body
* **POST /users/** - Creates a new user with the specified details - Expects a JSON body  
* 
* **PUT /departments/{{id}}** - Updates an existing department - Expects a JSON body
* **PUT /teams/{{id}}** - Updates an existing team - Expects a JSON body
* **PUT /users/{{id}}** - Updates an existing user - Expects a JSON body  

You are recommended to use **AWS** and to present a Solution Architecture though you are also welcome to create a **MERN** stack to implement your backend and API layer. You are free to use a different language/framework/libraries you are comfortable with.

## Frontend implementation
Implement a small frontend application to consume the API you developed above.  

The frontend should be able to show a list of names of the departments available in the database. 
Upon clicking the name of a department on the list, the user should be navigated to a more detailed view of the selected department, where they are presented with the teams and department in-charge. 
Once they click a team, they are presented with the team lead, and all the users inside the team. 
You should also implement forms where the user is able to create/update departments, teams, and users (using the POST and PUT endpoints)  

You are recommended to use **ReactJS** to create the frontend, but you are free to use a different JavaScript framework.

### Notes and recommendations
* The languages, frameworks and libraries mentioned are recommendations only, you are free to use whatever you are comfortable with.
* The project structure is up to you to decide
* You are recommended to use git commits along with commenting in a logical manner to demonstrate the development progress
* Writing tests and adhering to development standards/conventions leaves a positive impression  

## **Submission :**

- Fork the project into a private bitbucket repo
- Complete the tasks in steps and merge your pull requests(with adequate descriptions) to master
- Add femi@divercity.io and chuka@divercity.io to the repo so we can review your code
- Email to andrew@divercity.io once finished and ready for the review  

## **Bonus :**
- Bonus: Deploy your application to an AWS Account
- Bonus: Use MermaidJS to show a Diagram of your Application
- Bonus: Use MermaidJS to show a Solution Architecture of your Back-End Architecture
- Bonus: Add a Markdown based Readme file to your Repo and explain what the repo does and how others are welcome to fork it to improve upon the code while using Git icons and implementations to draw attention to mastery of Markdown


